IMAGE=davisd/go
TAG=$(shell git rev-parse --abbrev-ref HEAD)

ifneq ($(BITBUCKET_BRANCH),)
TAG=$(BITBUCKET_BRANCH)
endif

ifneq ($(BITBUCKET_TAG),)
TAG=$(BITBUCKET_TAG)
endif

push: docker
	docker tag $(IMAGE):latest $(IMAGE):$(TAG)
	docker push $(IMAGE):$(TAG)

deploy: push
	docker push $(IMAGE):latest

docker:
	docker build --build-arg linter=$(LINTER_VERSION) -t $(IMAGE) .

default: push
