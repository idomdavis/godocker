FROM golang:alpine

# Use LINTER_VERSION in bitbucket to override this
ARG linter=v1.60.1

RUN set -x \
  && apk -Uuv add openssh git make gcc libc-dev zip curl\
  && rm /var/cache/apk/* \
  && mkdir -p /root/.ssh \
  && ssh-keyscan bitbucket.org > /root/.ssh/known_hosts \
  && git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/" \
  && wget -O - -q \
       https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh \
       | sh -s $linter \
  && sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin
