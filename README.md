# Golang Docker Image

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/godocker/main?style=plastic)](https://bitbucket.org/idomdavis/godocker/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/godocker?style=plastic)](https://bitbucket.org/idomdavis/godocker/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/godocker?style=plastic)](https://bitbucket.org/idomdavis/godocker/pull-requests/)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

Built on top of the official Alpine go image, this adds a few extra tools used
by derivatives of [Go Base][gobase].

To push a new version of the image run

```bash
make push
```

To update the `latest` tag run

```bash
make deploy
```

[gobase]: https://bitbucket.org/idomdavis/gobase
